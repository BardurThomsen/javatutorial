/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatutorial;

import java.util.Scanner;

/**
 *
 * @author Bardur Thomsen <https://github.com/bardurt>
 */
public class TutorialOne {
    
    
    
    
    public void run(){
    
        int a = 1;
        int b = 2;
        int c = a + b;
        
        
        Scanner scanner;
        scanner = new Scanner(System.in);
        
        System.out.println("Please write your name");
        
        String name = scanner.nextLine();
        
        
        System.out.println("Hello " + name);
        
        
        System.out.println(a + " + " + b + " = " + c);
    }
}
