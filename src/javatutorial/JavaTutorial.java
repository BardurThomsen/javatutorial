/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatutorial;

/**
 *
 * @author Bardur Thomsen <https://github.com/bardurt>
 */
public class JavaTutorial {

    int a; // integer value: -2,-1,0,1,....., a whole number
    double d; // a real number: 1, 0.3, 1.333, 0.245, any number with decimal
    float f; // same as double, but with less precision
    char c; // a character: "a","b", "5", "3" , ".",
    
    /**
     * This is the main method for the entire application.
     * This method is the start point.
     * @param args arguments for starting the Java application.
     */
    public static void main(String[] args) {
    
       TutorialOne app = new TutorialOne();
       
       app.run();
    }
    
}
